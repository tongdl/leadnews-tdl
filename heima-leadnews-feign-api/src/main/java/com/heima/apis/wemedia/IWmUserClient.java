package com.heima.apis.wemedia;

import com.heima.model.common.dtos.ResponseResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author itheima
 * @since 2022-07-17
 */
@FeignClient("leadnews-wemedia")
public interface IWmUserClient {

    @PostMapping("/wemedia/user/add")
    ResponseResult addUser(@RequestBody Map<String, String> param);

}
