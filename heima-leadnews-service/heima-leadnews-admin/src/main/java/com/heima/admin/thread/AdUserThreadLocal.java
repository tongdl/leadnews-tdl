package com.heima.admin.thread;

import com.heima.model.admin.pojos.AdUser;

/**
 * @author itheima
 * @since 2022-07-10
 */
public class AdUserThreadLocal {

    // 1. 声明一个ThreadLocal类型的变量
    public static ThreadLocal<AdUser> userThreadLocal = new ThreadLocal<>();

    // 2. 为这个变量配置一对 get set方法
    public static AdUser get() {
        return userThreadLocal.get();
    }

    public static void set(AdUser wmUser) {
        userThreadLocal.set(wmUser);
    }

    // 3. 为这个变量配置一个 remove 方法
    public static void remove() {
        userThreadLocal.remove();
    }

}
