package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.dtos.ArticleInfoDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.vos.ArticleVisitStreamMess;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.http.ResponseEntity;

public interface ApArticleService extends IService<ApArticle> {
    ResponseResult load(ArticleHomeDto dto);

    ResponseResult load2(ArticleHomeDto dto);

    ResponseResult loadmore(ArticleHomeDto dto);

    ResponseResult loadnew(ArticleHomeDto dto);

    void generate(Long articleId);

    ResponseResult saveArticle(ArticleDto dto);

    void sendIndexUpdate(ApArticle article, String content);

    ResponseResult loadArticleBehavior(ArticleInfoDto dto);

    void updateScore(ArticleVisitStreamMess mess);
}