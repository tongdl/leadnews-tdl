package com.heima.article.listener;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.model.article.pojos.ApArticleConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author itheima
 * @since 2022-07-23
 */
//@Component
public class ArticleListener {

    @Autowired
    private ApArticleConfigMapper apArticleConfigMapper;

//    @KafkaListener(topics = "news.up.or.down")
    public void getMessage(String message) {
        if (StringUtils.isBlank(message)) {
            return;
        }

        Map map = JSON.parseObject(message, Map.class);

        String articleId = (String) map.get("articleId");
        Short enable = (Short) map.get("enable");


        ApArticleConfig config = new ApArticleConfig();
        if (1 == enable) {
            config.setIsDown(false);
        } else {
            config.setIsDown(true);
        }

        LambdaQueryWrapper<ApArticleConfig> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(ApArticleConfig::getArticleId, articleId);
        apArticleConfigMapper.update(config, wrapper);
    }
}
