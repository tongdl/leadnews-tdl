package com.heima.article.listener;

import com.alibaba.fastjson.JSON;
import com.heima.article.service.ApArticleService;
import com.heima.model.article.vos.ArticleVisitStreamMess;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author itheima
 * @since 2022-07-31
 */
@Component
@Slf4j
public class ArticleHotListener {

    @Autowired
    private ApArticleService apArticleService;

    @KafkaListener(topics = "itcast-topic-out")
    public void articleHot(String message) {
        System.out.println("收到流式处理的消息了");
        System.out.println(message);
        if (StringUtils.isBlank(message)) {
            return;
        }

        ArticleVisitStreamMess mess = JSON.parseObject(message, ArticleVisitStreamMess.class);
        if (mess == null) {
            return;
        }

        apArticleService.updateScore(mess);
    }
}
