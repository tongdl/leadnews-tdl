package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Mapper
@Repository
public interface ApArticleMapper extends BaseMapper<ApArticle> {

    /**
     * 获取文章列表
     *
     * @param dto      查询条件
     * @param loadType 1代表小于minBehotTime，2代表大于maxBehotTime
     * @return List
     */
    List<ApArticle> loadArticle(@Param("dto") ArticleHomeDto dto, @Param("loadType") Integer loadType);

    List<ApArticle> findArticleByLast5days(@Param("dateParam") LocalDateTime dateParam);

}