package com.heima.article.service;

/**
 * @author itheima
 * @since 2022-07-29
 */
public interface HotArticleService {
    void computeHotArticle();
}
