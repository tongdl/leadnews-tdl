package com.heima.article;

import com.heima.article.service.HotArticleService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author itheima
 * @since 2022-07-24
 */
@SpringBootTest
public class ArticleComputeTest {

    @Autowired
    private HotArticleService hotArticleService;

    @Test
    public void computeTest() {
        hotArticleService.computeHotArticle();
    }

}
