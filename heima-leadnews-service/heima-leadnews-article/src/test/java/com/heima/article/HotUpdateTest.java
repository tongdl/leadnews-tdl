package com.heima.article;

import com.heima.article.service.ApArticleService;
import com.heima.model.article.vos.ArticleVisitStreamMess;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author itheima
 * @since 2022-07-31
 */
@SpringBootTest
public class HotUpdateTest {

    @Autowired
    private ApArticleService apArticleService;

    @Test
    public void test() {
        ArticleVisitStreamMess mess = new ArticleVisitStreamMess();
        mess.setArticleId(1552L);
        mess.setView(0);
        mess.setComment(0);
        mess.setCollect(0);
        mess.setLike(2);
        apArticleService.updateScore(mess);

    }


}
