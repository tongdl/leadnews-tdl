package com.heima.article;

import com.heima.article.service.ApArticleService;
import com.heima.model.article.pojos.ApArticle;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * @author itheima
 * @since 2022-07-25
 */
@SpringBootTest
public class EsTest {

    @Autowired
    private ApArticleService apArticleService;

    @Test
    public void test() {
        ApArticle article = new ApArticle();

        article.setId(10000L);
        article.setStaticUrl("http://www.heima.com");
        article.setAuthorId(1001L);
        article.setAuthorName("小黑");
        article.setLayout((short) 1);
        article.setPublishTime(new Date());
        article.setTitle("我是小黑");

        apArticleService.sendIndexUpdate(article, "123456");

    }
}
