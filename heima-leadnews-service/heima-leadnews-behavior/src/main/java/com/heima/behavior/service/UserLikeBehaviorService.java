package com.heima.behavior.service;

import com.heima.model.behavoir.LikesBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author itheima
 * @since 2022-07-28
 */
public interface UserLikeBehaviorService {
    ResponseResult likesBehavior(@RequestBody LikesBehaviorDto dto);
}
