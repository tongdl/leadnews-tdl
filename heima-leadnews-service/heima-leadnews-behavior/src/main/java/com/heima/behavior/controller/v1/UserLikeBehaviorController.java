package com.heima.behavior.controller.v1;

import com.heima.behavior.service.UserLikeBehaviorService;
import com.heima.model.behavoir.LikesBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author itheima
 * @since 2022-07-28
 */
@RestController
@RequestMapping("/api/v1")
public class UserLikeBehaviorController {

    @Autowired
    private UserLikeBehaviorService userLikeBehaviorService;

    @PostMapping("/likes_behavior")
    public ResponseResult likesBehavior(@RequestBody LikesBehaviorDto dto) {
        return userLikeBehaviorService.likesBehavior(dto);
    }
}
