package com.heima.behavior.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.behavior.service.UserLikeBehaviorService;
import com.heima.behavior.thread.AppThreadLocalUtil;
import com.heima.common.cache.CacheService;
import com.heima.common.constants.BehaviorConstants;
import com.heima.model.behavoir.LikesBehaviorDto;
import com.heima.model.behavoir.UpdateArticleMess;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * @author itheima
 * @since 2022-07-28
 */
@Service
@Slf4j
public class UserLikeBehaviorServiceImpl implements UserLikeBehaviorService {

    @Autowired
    private CacheService cacheService;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Override
    public ResponseResult likesBehavior(LikesBehaviorDto dto) {
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        ApUser user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
        }

        Integer userId = user.getId();
        if (userId == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
        }

        Short operation = dto.getOperation();
        Long articleId = dto.getArticleId();

        if (0 == operation) {

            // 点赞消息
            UpdateArticleMess updateArticleMess = new UpdateArticleMess();
            updateArticleMess.setAdd(1);
            updateArticleMess.setArticleId(articleId);
            updateArticleMess.setType(UpdateArticleMess.UpdateArticleType.LIKES);

            kafkaTemplate.send("itcast-topic-in", JSON.toJSONString(updateArticleMess));

            // 去重
            Object likeBehavior = cacheService.hGet(BehaviorConstants.LIKE_BEHAVIOR + articleId, userId.toString());
            if (likeBehavior != null) {
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST, "不可重复点赞");
            }

            // 写入数据
            long currentTimeMillis = System.currentTimeMillis();
            cacheService.hPut(BehaviorConstants.LIKE_BEHAVIOR + articleId, userId.toString(), Long.toString(currentTimeMillis));
        } else {
            cacheService.hDelete(BehaviorConstants.LIKE_BEHAVIOR + articleId, userId.toString());
            // 取消点赞消息
            UpdateArticleMess updateArticleMess = new UpdateArticleMess();
            updateArticleMess.setAdd(-1);
            updateArticleMess.setArticleId(articleId);
            updateArticleMess.setType(UpdateArticleMess.UpdateArticleType.LIKES);

            kafkaTemplate.send("itcast-topic-in", JSON.toJSONString(updateArticleMess));
        }

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
