package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.heima.apis.wemedia.IWmUserClient;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.AuthDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.model.user.pojos.ApUserRealname;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.mapper.ApUserRealNameMapper;
import com.heima.user.service.ApUserRealNameService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;


/**
 * @author itheima
 * @since 2022-07-26
 */
@Service
@Slf4j
public class ApUserRealNameServiceImpl implements ApUserRealNameService {

    @Autowired
    private ApUserRealNameMapper apUserRealNameMapper;

    @Autowired
    private ApUserMapper apUserMapper;

    @Autowired
    private IWmUserClient iWmUserClient;

    @Override
    public PageResponseResult list(AuthDto dto) {
        if (dto == null) {
            return new PageResponseResult(0, 0, 0);
        }

        // 分页参数校验
        dto.checkParam();

        Integer page = dto.getPage();
        Integer size = dto.getSize();

        Page<ApUserRealname> pageInfo = new Page<>(page, size);

        LambdaQueryWrapper<ApUserRealname> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(dto.getStatus() != null, ApUserRealname::getStatus, dto.getStatus());

        Page<ApUserRealname> pageResult = apUserRealNameMapper.selectPage(pageInfo, wrapper);

        Long total = pageResult.getTotal();
        PageResponseResult pageResponseResult = new PageResponseResult(page, size, total.intValue());
        pageResponseResult.setData(pageResult.getRecords());

        return pageResponseResult;
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public ResponseResult auth(AuthDto dto, Integer type) {
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        Integer id = dto.getId();

        ApUserRealname apUserRealname = apUserRealNameMapper.selectById(id);
        if (apUserRealname == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        // 更新一下审核状态
        apUserRealname.setStatus((short) (type == 1 ? 9 : 2));
        apUserRealname.setId(id);
        apUserRealname.setReason(dto.getMsg());
        apUserRealNameMapper.updateById(apUserRealname);

        // 通过
        if (type == 1) {
            String name = apUserRealname.getName();

            Map<String, String> map = new HashMap<>();
            map.put("name", name);
            ResponseResult responseResult = iWmUserClient.addUser(map);
            if (responseResult == null || responseResult.getCode() != 200) {
                throw new RuntimeException("自媒体用户创建失败");
            }

            ApUser apUser = new ApUser();
            apUser.setId(apUserRealname.getUserId());
            apUser.setFlag((short) 1);
            int updateResult = apUserMapper.updateById(apUser);
            if (updateResult < 1) {
                throw new RuntimeException("APP用户状态更新失败");
            }
        }

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }
}
