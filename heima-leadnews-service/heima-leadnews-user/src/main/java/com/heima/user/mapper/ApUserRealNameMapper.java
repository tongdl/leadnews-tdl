package com.heima.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.user.pojos.ApUserRealname;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @author itheima
 * @since 2022-07-26
 */
@Mapper
@Repository
public interface ApUserRealNameMapper extends BaseMapper<ApUserRealname> {
}
