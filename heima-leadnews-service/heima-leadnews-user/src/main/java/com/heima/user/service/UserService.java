package com.heima.user.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.UserRelationDto;

/**
 * @author itheima
 * @since 2022-07-28
 */
public interface UserService {

    ResponseResult userFollow(UserRelationDto dto);

}
