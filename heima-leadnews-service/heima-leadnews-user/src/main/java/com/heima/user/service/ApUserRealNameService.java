package com.heima.user.service;

import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.AuthDto;

/**
 * @author itheima
 * @since 2022-07-26
 */
public interface ApUserRealNameService {

    /**
     * 查询认证列表
     *
     * @param dto dto
     * @return PageResponseResult
     */
    PageResponseResult list(AuthDto dto);

    /**
     * 通过/不通过
     *
     * @param dto  审核信息
     * @param type 1通过 2不通过
     * @return
     */
    ResponseResult auth(AuthDto dto, Integer type);

}
