package com.heima.schedule.service;

import com.heima.model.schedule.dto.Task;

/**
 * @author itheima
 * @since 2022-07-18
 */
public interface TaskService {

    /**
     * 添加任务
     *
     * @param task 任务信息
     * @return 任务id
     */
    Long addTask(Task task);

    Boolean cancelTask(Long taskId);

    /**
     * 消费任务
     *
     * @return Task
     */
    Task pollTask();

}
