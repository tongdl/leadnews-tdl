package com.heima.schedule.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.schedule.pojos.Taskinfo;

/**
 * @author itheima
 * @since 2022-06-29
 */
public interface TaskinfoService extends IService<Taskinfo> {
}
