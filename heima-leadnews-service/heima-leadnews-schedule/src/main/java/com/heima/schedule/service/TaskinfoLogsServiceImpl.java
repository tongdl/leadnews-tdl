package com.heima.schedule.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.schedule.pojos.TaskinfoLogs;
import com.heima.schedule.mapper.TaskinfoLogsMapper;
import org.springframework.stereotype.Service;

/**
 * @author itheima
 * @since 2022-06-29
 */
@Service
public class TaskinfoLogsServiceImpl extends ServiceImpl<TaskinfoLogsMapper, TaskinfoLogs> implements TaskinfoLogsService {
}
