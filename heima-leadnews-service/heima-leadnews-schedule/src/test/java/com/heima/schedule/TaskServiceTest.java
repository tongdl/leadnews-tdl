package com.heima.schedule;

import com.heima.model.schedule.dto.Task;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.schedule.service.TaskService;
import com.heima.utils.ProtostuffUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * @author itheima
 * @since 2022-07-26
 */
@SpringBootTest
public class TaskServiceTest {

    @Autowired
    private TaskService taskService;

    @Test
    public void TestAdd() {

        WmNews wmNews = new WmNews();
        wmNews.setId(6332);

        Task task = new Task();

        task.setExecuteTime(new Date().getTime());
        task.setParameters(ProtostuffUtil.serialize(wmNews));
        taskService.addTask(task);
    }

    @Test
    public void testPoll() {
        Task task = taskService.pollTask();
    }
}
