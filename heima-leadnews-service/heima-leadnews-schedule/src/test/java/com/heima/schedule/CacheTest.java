package com.heima.schedule;

import com.alibaba.fastjson.JSON;
import com.heima.common.cache.CacheService;
import com.heima.schedule.service.TaskService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author itheima
 * @since 2022-07-18
 */
@SpringBootTest
public class CacheTest {

    @Autowired
    private CacheService cacheService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void test() {
//
//        cacheService.set("itheima", "1");
//
//        String itheima = cacheService.get("itheima");
//
//        System.out.println(itheima);


        Map<String, Object> map = new HashMap<>();

        redisTemplate.opsForValue().set("key", JSON.toJSONString(map));


        Set<String> future = cacheService.zRangeByScore("FUTURE", 0, System.currentTimeMillis());

        cacheService.refreshWithPipeline("FUTURE", "TOPIC", future);

    }
}
