package com.heima.search.listener;

import com.alibaba.fastjson.JSON;
import com.heima.model.search.vos.SearchArticleVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @author itheima
 * @since 2022-07-25
 */
@Slf4j
@Component
public class ArticleIndexUpdateListener {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @KafkaListener(topics = "es.index.update")
    public void listener(String message) throws IOException {
        if (StringUtils.isBlank(message)) {
            return;
        }

        log.info("我收到消息啦:" + message);

        SearchArticleVo vo = JSON.parseObject(message, SearchArticleVo.class);

        IndexRequest indexRequest = new IndexRequest();
        indexRequest.id(vo.getId().toString()).source(JSON.toJSONString(vo));
        restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
    }
}
