package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

/**
 * @author itheima
 * @since 2022-07-25
 */
public interface AssociateService {

    ResponseResult search(UserSearchDto dto);
}
