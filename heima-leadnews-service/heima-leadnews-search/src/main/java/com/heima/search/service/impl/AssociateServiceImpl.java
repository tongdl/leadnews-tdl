package com.heima.search.service.impl;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.pojos.ApAssociateWords;
import com.heima.search.service.AssociateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author itheima
 * @since 2022-07-25
 */
@Service
@Slf4j
public class AssociateServiceImpl implements AssociateService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public ResponseResult search(UserSearchDto dto) {
        if (dto == null) {
            ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }

        String searchWords = dto.getSearchWords();
        int pageSize = dto.getPageSize();

        Query query = new Query();
        query.addCriteria(Criteria.where("associateWords").regex(".*?\\" + searchWords + ".*"));
        query.limit(pageSize);

        List<ApAssociateWords> list = mongoTemplate.find(query, ApAssociateWords.class);
        return ResponseResult.okResult(list);
    }
}
