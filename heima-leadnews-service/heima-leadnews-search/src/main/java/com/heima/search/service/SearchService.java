package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

import java.io.IOException;

/**
 * @author itheima
 * @since 2022-07-25
 */
public interface SearchService {

    ResponseResult search(UserSearchDto dto) throws IOException;

}
