package com.heima.search.service.impl;

import com.heima.common.search.ElasticSearchTemplate;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.search.service.ApUserSearchService;
import com.heima.search.service.SearchService;
import com.heima.search.thread.AppThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.*;

/**
 * @author itheima
 * @since 2022-07-25
 */
@Service
@Slf4j
public class SearchServiceImpl implements SearchService {

    @Autowired
    private ElasticSearchTemplate elasticSearchTemplate;

    @Autowired
    private ApUserSearchService apUserSearchService;

    @Override
    public ResponseResult search(UserSearchDto dto) throws IOException {

        int pageNum = dto.getPageNum();
        int pageSize = dto.getPageSize();

        String searchWords = dto.getSearchWords();
        Date minBehotTime = dto.getMinBehotTime();

        ApUser user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
        }

        Integer userId = user.getId();
        if (userId == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
        }

        // 1. 字符串搜索
        // 在 title = 关键字 or content = 关键字
        QueryStringQueryBuilder stringQueryBuilder = elasticSearchTemplate.getStringQueryBuilder(searchWords, Operator.OR, "title", "content");

        // 2. 日期的范围搜索
        Map<String, Object> condition = new HashMap<>();
        condition.put("lt", minBehotTime);
        RangeQueryBuilder rangeQueryBuilder = elasticSearchTemplate.getRangeBuilder("publishTime", condition);

        // 3. 布尔查询
        Map<String, AbstractQueryBuilder> boolQueryCondition = new HashMap<>();
        boolQueryCondition.put("must", stringQueryBuilder);
        boolQueryCondition.put("filter", rangeQueryBuilder);
        BoolQueryBuilder boolQueryBuilder = elasticSearchTemplate.getBoolQueryBuilder(boolQueryCondition);

        // 4. 高亮
        // "<font style='color: red; font-size: inherit;'>title</font>"
        HighlightBuilder highlightBuilder = elasticSearchTemplate.getHighlightBuilder("title", "<font style='color: red; font-size: inherit;'>", "</font>");

        // 6. 执行搜索
        SearchResponse search = elasticSearchTemplate.search("app_info_article", boolQueryBuilder, highlightBuilder, pageNum, pageSize, "publishTime", SortOrder.DESC);

        // 7. 记录本次搜索的关键字
        apUserSearchService.insert(searchWords, userId);

        SearchHits hits = search.getHits();
        SearchHit[] hits1 = hits.getHits();
        if (ArrayUtils.isEmpty(hits1)) {
            return ResponseResult.okResult("");
        }

        List<Map<String, Object>> result = new ArrayList<>();
        for (SearchHit hit : hits1) {
            if (hit == null) {
                continue;
            }


            Map<String, Object> sourceAsMap = hit.getSourceAsMap();

            Map<String, HighlightField> highlightFields = hit.getHighlightFields();

            // 处理高亮显示
            if (!CollectionUtils.isEmpty(highlightFields)) {
                Text[] titles = highlightFields.get("title").getFragments();
                String join = StringUtils.join(titles, ",");
                sourceAsMap.put("h_title", join);
            } else {
                sourceAsMap.put("h_title", sourceAsMap.get("title"));
            }

            result.add(sourceAsMap);
        }

        return ResponseResult.okResult(result);
    }

}
