package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.HistorySearchDto;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author itheima
 * @since 2022-07-25
 */
public interface ApUserSearchService {
    void insert(String keyword, Integer userId);

    ResponseResult load();

    ResponseResult delUserSearch(@RequestBody HistorySearchDto historySearchDto);
}
