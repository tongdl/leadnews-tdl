package com.heima.search.service.impl;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.HistorySearchDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.search.pojos.ApUserSearch;
import com.heima.search.service.ApUserSearchService;
import com.heima.search.thread.AppThreadLocalUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author itheima
 * @since 2022-07-25
 */
@Service
@Slf4j
public class ApUserSearchServiceImpl implements ApUserSearchService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void insert(String keyword, Integer userId) {
        if (userId == null) {
            log.warn("用户id不能为空");
            return;
        }

        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId).and("keyword").is(keyword));
        ApUserSearch searchHistory = mongoTemplate.findOne(query, ApUserSearch.class);
        if (searchHistory == null) {
            ApUserSearch search = new ApUserSearch();
            search.setUserId(userId);
            search.setKeyword(keyword);
            search.setCreatedTime(new Date());
            mongoTemplate.save(search);
        } else {
            searchHistory.setCreatedTime(new Date());
            mongoTemplate.save(searchHistory);
        }
    }

    @Override
    public ResponseResult load() {

        ApUser user = AppThreadLocalUtil.getUser();
        if (user == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
        }

        Integer userId = user.getId();
        if (userId == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.AP_USER_DATA_NOT_EXIST);
        }

        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        query.with(Sort.by("createdTime").descending());
        query.limit(10);

        List<ApUserSearch> list = mongoTemplate.find(query, ApUserSearch.class);

        return ResponseResult.okResult(list);
    }

    @Override
    public ResponseResult delUserSearch(HistorySearchDto historySearchDto) {
        if (historySearchDto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        String id = historySearchDto.getId();

        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));

        mongoTemplate.remove(query, ApUserSearch.class);

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

}
