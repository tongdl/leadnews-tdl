package com.heima.search;

import com.heima.search.service.ApUserSearchService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author itheima
 * @since 2022-07-25
 */
@SpringBootTest
public class SearchTest {

    @Autowired
    private ApUserSearchService apUserSearchService;

    @Test
    public void test() {
        apUserSearchService.insert("哈哈", 1001);
    }
}
