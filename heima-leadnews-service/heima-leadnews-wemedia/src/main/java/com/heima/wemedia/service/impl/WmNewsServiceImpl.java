package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.apis.article.IArticleClient;
import com.heima.apis.schedule.IScheduleClient;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.schedule.dto.Task;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import com.heima.model.wemedia.vo.WmNewsVo;
import com.heima.utils.ProtostuffUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.service.AutoScanService;
import com.heima.wemedia.service.WmNewsService;
import com.heima.wemedia.thread.UserThreadLocal;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * 新闻
 *
 * @author tongdulong@itcast.cn
 */
@Service
@Slf4j
@Transactional
public class WmNewsServiceImpl extends ServiceImpl<WmNewsMapper, WmNews> implements WmNewsService {

    @Autowired
    private WmNewsMapper wmNewsMapper;

    @Autowired
    private WmMaterialMapper wmMaterialMapper;

    @Autowired
    private WmNewsMaterialMapper wmNewsMaterialMapper;

    @Autowired
    private IScheduleClient scheduleClient;

    @Autowired
    private IArticleClient iArticleClient;

    @Autowired
    private AutoScanService autoScanService;

    @Autowired
    private KafkaTemplate kafkaTemplate;

    @Override
    public PageResponseResult newsList(WmNewsPageReqDto dto) {
        if (dto == null) {
            return new PageResponseResult(0, 0, 0);
        }

        Short status = dto.getStatus();
        Date beginPubDate = dto.getBeginPubDate();
        Date endPubDate = dto.getEndPubDate();
        Integer channelId = dto.getChannelId();
        String keyword = dto.getKeyword();
        Integer currentPage = dto.getPage();
        Integer size = dto.getSize();

        // 1. 构建查询条件
        LambdaQueryWrapper<WmNews> wrapper = new LambdaQueryWrapper<>();

        // 状态查询
        wrapper.eq(status != null, WmNews::getStatus, status);

        // 发布时间范围
        wrapper.between(beginPubDate != null && endPubDate != null, WmNews::getPublishTime, beginPubDate, endPubDate);

        // 频道查询
        wrapper.eq(channelId != null, WmNews::getChannelId, channelId);

        // 关键字(模糊查询）
        wrapper.like(StringUtils.isNotEmpty(keyword), WmNews::getTitle, keyword);

        // 查询当前登录用户的文章
        wrapper.eq(WmNews::getUserId, UserThreadLocal.get().getId());

        // 根据发布时间进行降序排序
        wrapper.orderByDesc(WmNews::getPublishTime);

        // 3. 构建分页条件
        Page<WmNews> page = new Page<>(currentPage, size);

        // 4. 执行分页查询
        Page<WmNews> wmNewsPageResult = wmNewsMapper.selectPage(page, wrapper);

        // 5. 构建返回值并返回
        Long total = wmNewsPageResult.getTotal();
        PageResponseResult pageResult = new PageResponseResult(currentPage, size, total.intValue());
        pageResult.setData(wmNewsPageResult.getRecords());

        return pageResult;
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public ResponseResult submit(WmNewsDto wmNewsDto) {
        // 1. 提取新闻内容
        WmNews wmNews = wmNewsDto2WmNews(wmNewsDto);
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        // 4. 提取新闻图片（素材）
        String content = wmNewsDto.getContent();
        List<String> contentImages = getContentImages(content);

        // 5. 如果是自动布局，要到内容图中，去提取图片作为封面图
        List<String> images = wmNewsDto.getImages();
        Short type = wmNewsDto.getType();
        List<String> coverImages = getCoverImages(type, images, contentImages);

        // 如果是自动布局，就根据自动提取的封面图设置type字段
        if (wmNewsDto.getType() == -1) {
            if (coverImages.size() == 3) {
                wmNews.setType((short) 3);
                // 0726 添加封面图回写逻辑
                wmNews.setImages(StringUtils.join(coverImages.subList(0, 3), ","));
            } else if (coverImages.size() < 3 && contentImages.size() > 0) {
                wmNews.setType((short) 1);
                // 0726 添加封面图回写逻辑
                wmNews.setImages(coverImages.get(0));
            } else {
                wmNews.setType((short) 0);
            }
        }

        // 2. 插入新闻表
        int insertResult = wmNewsMapper.insert(wmNews);
        if (insertResult < 1) {
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
        }

        // 3. 判断是不是草稿（是草稿->返回）
        Short status = wmNewsDto.getStatus();
        if (status == 0) {
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }

        // 6. 素材添加
        Boolean contentImageResult = addMaterial(wmNews.getId(), contentImages, 0);
        if (!contentImageResult) {
            throw new RuntimeException("内容图添加失败");
        }

        // 7. 素材和新闻关系添加
        Boolean coverImageResult = addMaterial(wmNews.getId(), coverImages, 1);
        if (!coverImageResult) {
            throw new RuntimeException("封面图添加失败");
        }

        //8. 调用审核代码
//        try {
//            autoScanService.autoScanWmNews(wmNews.getId().longValue());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        Boolean addTaskResult = addToTask(wmNews.getId(), wmNews.getPublishTime().getTime());
        if (!addTaskResult) {
            throw new RuntimeException("任务添加失败");
        }

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }


    @Override
    public ResponseResult downOrUp(WmNewsDto wmNewsDto) {
        if (wmNewsDto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        Integer newsId = wmNewsDto.getId();
        WmNews wmNews = wmNewsMapper.selectById(newsId);
        if (wmNews == null) {
            log.warn("新闻不存在");
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "新闻不存在");
        }

        Short status = wmNews.getStatus();
        if (9 != status) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "新闻未发布");
        }

        // 修改状态，并更新
        wmNews.setEnable(wmNewsDto.getEnable());
        int updateResult = wmNewsMapper.updateById(wmNews);
        if (updateResult < 1) {
            log.warn("新闻更新失败");
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR, "新闻更新失败");
        }

        Map<String, Object> map = new HashMap<>();
        map.put("articleId", wmNews.getArticleId());
        map.put("enable", wmNews.getEnable());
        kafkaTemplate.send("news.up.or.down", JSON.toJSONString(map));

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    @Transactional(rollbackFor = RuntimeException.class)
    public ResponseResult passOrFail(NewsAuthDto dto, Integer type) {
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        Integer id = dto.getId();
        String msg = dto.getMsg();

        WmNews wmNews = wmNewsMapper.selectById(id);
        if (wmNews == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST, "新闻不存在");
        }

        int status = type == 1 ? 9 : 2;
        wmNews.setStatus((short) status);
        wmNews.setReason(type == 1 ? "通过" : msg);
        int updateResult = wmNewsMapper.updateById(wmNews);
        if (updateResult < 1) {
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR, "新闻审核失败");
        }

        if (type == 1) {
            ArticleDto articleDto = autoScanService.wmNews2ArticleDto(wmNews);
            ResponseResult responseResult = iArticleClient.saveArticle(articleDto);
            if (responseResult == null || responseResult.getCode() != 200) {
                throw new RuntimeException("文章保存失败");
            }
        }

        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }

    @Override
    public ResponseResult findList(NewsAuthDto dto) {
        //1.参数检查
        dto.checkParam();

        //记录当前页
        int currentPage = dto.getPage();

        //2.分页查询+count查询
        dto.setPage((dto.getPage() - 1) * dto.getSize());
        List<WmNewsVo> wmNewsVoList = wmNewsMapper.findListAndPage(dto);
        int count = wmNewsMapper.findListCount(dto);

        //3.结果返回
        ResponseResult responseResult = new PageResponseResult(currentPage, dto.getSize(), count);
        responseResult.setData(wmNewsVoList);
        return responseResult;
    }

    private Boolean addToTask(Integer newsId, Long executeTime) {
        Task task = new Task();
        task.setExecuteTime(executeTime);

        WmNews wmNews = new WmNews();
        wmNews.setId(newsId);

        task.setParameters(ProtostuffUtil.serialize(wmNews));

        ResponseResult responseResult = scheduleClient.addTask(task);
        if (responseResult != null && responseResult.getCode() == 200) {
            return true;
        }

        return false;
    }

    @Transactional(rollbackFor = RuntimeException.class)
    public Boolean addMaterial(Integer newsId, List<String> imageList, Integer type) {
        if (CollectionUtils.isEmpty(imageList)) {
            return true;
        }

        for (String url : imageList) {
            if (StringUtils.isEmpty(url)) {
                continue;
            }

            WmMaterial material = new WmMaterial();
            material.setUserId(UserThreadLocal.get().getId());
            material.setType((short) 0);
            material.setUrl(url);
            material.setIsCollection((short) 0);
            material.setCreatedTime(new Date());
            int insertResult = wmMaterialMapper.insert(material);
            if (insertResult < 1) {
                throw new RuntimeException("素材添加失败");
            }

            WmNewsMaterial newsMaterial = new WmNewsMaterial();
            newsMaterial.setType(type.shortValue());
            newsMaterial.setOrd((short) 1);
            newsMaterial.setNewsId(newsId);
            newsMaterial.setMaterialId(material.getId());
            int insertResult2 = wmNewsMaterialMapper.insert(newsMaterial);
            if (insertResult2 < 1) {
                throw new RuntimeException("素材关系添加失败");
            }
        }

        return true;
    }

    private List<String> getCoverImages(Short type, List<String> images, List<String> contentImages) {
        // 自动布局
        if (type == -1) {
            // 判断内容图数量
            // >= 3 取三张
            int size = contentImages.size();
            if (size >= 3) {
                return contentImages.subList(0, 3);
            } else if (size == 2) {
                return contentImages.subList(0, 1);
            } else if (size == 1) {
                List<String> list = new ArrayList<>();
                list.add(contentImages.get(0));
                return list;
            } else {
                return new ArrayList<>();
            }
        }

        return images;
    }

    public WmNews wmNewsDto2WmNews(WmNewsDto wmNewsDto) {
        if (wmNewsDto == null) {
            return null;
        }

        WmNews wmNews = new WmNews();

        // 自动映射
        BeanUtils.copyProperties(wmNewsDto, wmNews);

        // 处理额外的字段
        wmNews.setUserId(UserThreadLocal.get().getId());
        wmNews.setCreatedTime(new Date());
        wmNews.setEnable((short) 1);

        if (wmNewsDto.getType() == -1) {
            wmNews.setType(null);
        }

        List<String> images = wmNewsDto.getImages();
        if (!CollectionUtils.isEmpty(images)) {
            String join = StringUtils.join(images, ",");
            wmNews.setImages(join);
        }

        return wmNews;
    }

    private List<String> getContentImages(String content) {
        if (StringUtils.isEmpty(content)) {
            return new ArrayList<>();
        }

        List<String> urlList = new ArrayList<>();

        // 1. 将content转为集合
        List<Map> maps = JSON.parseArray(content, Map.class);
        if (CollectionUtils.isEmpty(maps)) {
            return new ArrayList<>();
        }

        // 2. 将集合进行遍历
        for (Map item : maps) {
            if (CollectionUtils.isEmpty(item)) {
                continue;
            }

            // 3. 判断type = images那行数据
            Object type = item.get("type");
            if ("image".equals(type)) {
                String value = (String) item.get("value");
                urlList.add(value);
            }
        }

        return urlList;
    }

}
