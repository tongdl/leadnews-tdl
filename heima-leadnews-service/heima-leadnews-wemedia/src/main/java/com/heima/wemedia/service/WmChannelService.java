package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.ChannelDto;
import com.heima.model.wemedia.pojos.WmChannel;
import org.springframework.http.ResponseEntity;

public interface WmChannelService extends IService<WmChannel> {

    ResponseResult channelList();

    ResponseResult channelSave(WmChannel channel);

    PageResponseResult channelList2(ChannelDto dto);

    ResponseResult channelUpdate(WmChannel wmChannel);

    ResponseResult channelDelete(Integer id);
}