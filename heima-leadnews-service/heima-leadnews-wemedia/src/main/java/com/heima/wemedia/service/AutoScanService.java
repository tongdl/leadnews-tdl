package com.heima.wemedia.service;

import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.wemedia.pojos.WmNews;

/**
 * @author itheima
 * @since 2022-07-17
 */
public interface AutoScanService {

    void autoScanWmNews(Long newsId) throws Exception;

    ArticleDto wmNews2ArticleDto(WmNews wmNews);

    /**
     * 消费延迟队列数据
     */
    void getTask();
}
