package com.heima.wemedia.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.NewsAuthDto;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmNews;

public interface WmNewsService extends IService<WmNews> {

    PageResponseResult newsList(WmNewsPageReqDto dto);

    ResponseResult submit(WmNewsDto wmNewsDto);

    ResponseResult downOrUp(WmNewsDto wmNewsDto);

    /**
     * @param dto
     * @param type 1:通过 2:不通过
     * @return
     */
    ResponseResult passOrFail(NewsAuthDto dto, Integer type);

    /**
     * 查询文章列表
     *
     * @param dto
     * @return
     */
    ResponseResult findList(NewsAuthDto dto);
}
