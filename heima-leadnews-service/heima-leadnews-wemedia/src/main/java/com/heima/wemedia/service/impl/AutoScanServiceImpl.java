package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.heima.apis.article.IArticleClient;
import com.heima.apis.schedule.IScheduleClient;
import com.heima.common.aliyun.GreenImageScan;
import com.heima.common.aliyun.GreenTextScan;
import com.heima.file.MinIoTemplate;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dto.Task;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmSensitive;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.OcrUtil;
import com.heima.utils.ProtostuffUtil;
import com.heima.utils.SensitiveWordUtil;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmSensitiveMapper;
import com.heima.wemedia.service.AutoScanService;
import com.heima.wemedia.service.WmChannelService;
import com.heima.wemedia.service.WmUserService;
import lombok.extern.slf4j.Slf4j;
import net.sourceforge.tess4j.TesseractException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author itheima
 * @since 2022-07-17
 */
@Service
@Slf4j
public class AutoScanServiceImpl implements AutoScanService {

    @Autowired
    private WmNewsMapper wmNewsMapper;

    @Autowired
    private WmChannelService wmChannelService;

    @Autowired
    private WmUserService wmUserService;

    @Autowired
    private WmSensitiveMapper wmSensitiveMapper;

    @Autowired
    private MinIoTemplate minIoTemplate;

    @Autowired
    private GreenTextScan greenTextScan;

    @Autowired
    private GreenImageScan greenImageScan;

    @Autowired
    private IArticleClient articleClient;

    @Override
    @Async
    public void autoScanWmNews(Long newsId) throws Exception {

        // 0. 入参判空
        if (newsId == null) {
            log.warn("newsId为空");
            return;
        }

        // 1. 查询新闻信息
        // 1.1 新闻为空 -> 响应失败
        WmNews wmNews = wmNewsMapper.selectById(newsId);
        if (wmNews == null) {
            log.warn("新闻" + newsId + "为空");
            return;
        }

        // 2. 提取新闻文字
        String title = wmNews.getTitle();
        String content = wmNews.getContent();
        String text = getText(title, content);

        // 3. 提取新闻图片
        String images = wmNews.getImages();
        List<byte[]> imageList = getImages(images, content);

        // 0. 通过OCR提取图片中的文字
        String textFromOcr = ocrCheck(imageList);
        text = text + "_" + textFromOcr;

        // 0. 使用DFA算法进行自建敏感词处理
        Boolean dfaResult = dfaCheck(text);
        if (!dfaResult) {
            // DFA检测不通过的话，需要更新一下文章状态
            updateWmNews(2, "DFA审核没通过", null, newsId.intValue());
            return;
        }

        // 4. 调用文字审核接口
        // 4.1 审核失败 -> wmNews修改方法
        // 4.2 审核不确定 -> wmNews修改方法
        Map textScan = greenTextScan.greeTextScan(text);
        Boolean textScanResult = checkResult(textScan, newsId.intValue());
        if (!textScanResult) {
            log.warn("文字审核失败");
            return;
        }

        // 5. 调用图片审核接口
        // 5.1 审核失败 -> wmNews修改方法
        // 5.2 审核不确定 -> wmNews修改方法
        Map imageScan = greenImageScan.imageScan(imageList);
        Boolean imageScanResult = checkResult(imageScan, newsId.intValue());
        if (!imageScanResult) {
            log.warn("图片审核失败");
            return;
        }

        // 6. 远程调用ArticleClient保存文章
        // 6.1 判断响应结果是不是200
        ArticleDto dto = wmNews2ArticleDto(wmNews);
        ResponseResult responseResult = articleClient.saveArticle(dto);
        if (responseResult == null || responseResult.getCode() != 200) {
            log.warn("文章保存失败");
            return;
        }

        Object data = responseResult.getData();
        if (data == null) {
            log.warn("文章保存失败，响应内容为空");
            return;
        }

        // 7. wmNews回写articleId，并修改状态为“通过”
        updateWmNews(9, "审核通过", (Long) data, newsId.intValue());
    }

    /**
     * 接收图片，提取图片里面的文字，并返回
     *
     * @param images 待检测的图片
     * @return 从图片中提取的文字
     */
    private String ocrCheck(List<byte[]> images) throws IOException, TesseractException {
        if (CollectionUtils.isEmpty(images)) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        for (byte[] image : images) {
            // 调用ocr工具类，提取里面的文字内容
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(image);
            BufferedImage read = ImageIO.read(byteArrayInputStream);

            String text = OcrUtil.doOcr(read);

            // 将文字内容组合成一个字符串，最终返回
            builder.append("_").append(text);
        }

        return builder.toString();
    }

    /**
     * DFA算法检测敏感词
     *
     * @param text 待检测的文字
     * @return 是否通过检测（true为通过，false为不通过）
     */
    private Boolean dfaCheck(String text) {
        if (StringUtils.isBlank(text)) {
            return true;
        }

        // 0.1 调用敏感词Mapper，查询所有敏感词
        QueryWrapper<WmSensitive> wrapper = new QueryWrapper<>();
        wrapper.select("sensitives");
        List<WmSensitive> wmSensitives = wmSensitiveMapper.selectList(wrapper);
        if (CollectionUtils.isEmpty(wmSensitives)) {
            return true;
        }

        List<String> words = wmSensitives.stream().filter(Objects::nonNull).map(WmSensitive::getSensitives).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(words)) {
            return true;
        }

        // 0.2 DFA初始化
        SensitiveWordUtil.initMap(words);

        // 0.3 DFA敏感词匹配
        Map<String, Integer> result = SensitiveWordUtil.matchWords(text);

        // 0.4 判断结果
        return CollectionUtils.isEmpty(result);
    }

    @Override
    public ArticleDto wmNews2ArticleDto(WmNews wmNews) {
        if (wmNews == null) {
            return null;
        }

        ArticleDto dto = new ArticleDto();
        BeanUtils.copyProperties(wmNews, dto);

        dto.setLayout(wmNews.getType());

        // 查询渠道的名称
        Integer channelId = wmNews.getChannelId();
        if (channelId != null) {
            WmChannel channel = wmChannelService.getById(channelId);
            if (channel != null) {
                dto.setChannelName(channel.getName());
            }
        }

        // 获取作者名
        Integer userId = wmNews.getUserId();
        if (userId != null) {
            dto.setAuthorId(userId.longValue());
            WmUser wmUser = wmUserService.getById(userId);
            if (wmUser != null) {
                dto.setAuthorName(wmUser.getName());
            }
        }

        return dto;
    }

    /**
     * 更新新闻状态
     *
     * @param status    状态
     * @param reason    审核结果
     * @param articleId 文章id
     * @param newsId    新闻id（查询条件）
     * @return 是否成功
     */
    private Boolean updateWmNews(Integer status, String reason, Long articleId, Integer newsId) {
        if (newsId == null || status == null) {
            log.warn("更新状态失败，入参缺失");
            return false;
        }

        WmNews wmNews = new WmNews();
        wmNews.setId(newsId);
        wmNews.setStatus(status.shortValue());
        wmNews.setReason(reason);
        if (articleId != null) {
            wmNews.setArticleId(articleId);
        }

        return wmNewsMapper.updateById(wmNews) > 0;
    }

    /**
     * 处理阿里云审核结果
     *
     * @param result 审核结果
     * @param newsId 新闻id
     * @return 是否成功
     */
    private Boolean checkResult(Map result, Integer newsId) {
        if (CollectionUtils.isEmpty(result)) {
            log.warn("审核结果为空");
            return true;
        }

        String suggestion = (String) result.get("suggestion");
        if ("pass".equals(suggestion)) {
            return true;
        } else if ("block".equals(suggestion)) {
            // 更新wmNews状态
            updateWmNews(2, "阿里云审核未通过", null, newsId);
            return false;
        } else if ("review".equals(suggestion)) {
            updateWmNews(3, "转人工审核", null, newsId);
            return false;
        }

        return true;
    }

    /**
     * 提取图片
     *
     * @param images  封面图
     * @param content 内容
     * @return 图片byte集合
     */
    private List<byte[]> getImages(String images, String content) {
        // 1.声明总的图片集合
        List<String> finalList = new ArrayList<>();

        // 2.提取封面图 —> List<String>
        if (StringUtils.isNotBlank(images)) {
            String[] split = images.split(",");
            if (ArrayUtils.isNotEmpty(split)) {
                List<String> list = Arrays.asList(split);

                // 加入到大的集合中
                finalList.addAll(list);
            }
        }

        // 3.提取内容图 -> List<String>
        if (StringUtils.isNotBlank(content)) {
            List<Map> maps = JSON.parseArray(content, Map.class);
            if (!CollectionUtils.isEmpty(maps)) {
                for (Map item : maps) {
                    if (CollectionUtils.isEmpty(item)) {
                        continue;
                    }

                    Object type = item.get("type");
                    if ("image".equals(type)) {
                        String value = (String) item.get("value");
                        finalList.add(value);
                    }
                }
            }
        }

        // 4.组装最后要输出的结果
        List<byte[]> result = new ArrayList<>();
        // 5.遍历图片集合，转成List<byte[]>
        if (!CollectionUtils.isEmpty(finalList)) {
            for (String url : finalList) {
                if (StringUtils.isEmpty(url)) {
                    continue;
                }

                // 使用MinIO的download方法，根据图片地址下载图片byte[]数据
                byte[] bytes = minIoTemplate.downLoadFile(url);
                result.add(bytes);
            }
        }

        // 6.返回集合
        return result;
    }

    /**
     * 提取新闻文字内容
     *
     * @param title   标题
     * @param content 内容
     * @return 组合后的字符串
     */
    private String getText(String title, String content) {
        // 1.入参校验
        if (StringUtils.isBlank(content)) {
            return title;
        }

        // 2.提取content里面的文字内容
        List<Map> maps = JSON.parseArray(content, Map.class);
        if (CollectionUtils.isEmpty(maps)) {
            return title;
        }

        // 3.组装内容文字
        StringBuilder builder = new StringBuilder();
        for (Map item : maps) {
            if (CollectionUtils.isEmpty(item)) {
                continue;
            }

            // 3.1提取文字内容
            Object type = item.get("type");
            if ("text".equals(type)) {
                String value = (String) item.get("value");
                builder.append(value);
            }
        }

        // 4.将content文字内容和title进行组合
        builder.append("_").append(title);

        // 5.返回组合后的结果
        return builder.toString();
    }

    @Autowired
    private IScheduleClient scheduleClient;

    @Scheduled(fixedRate = 1000)
    @Override
    public void getTask() {
        System.out.println("获取任务执行中=====");
        ResponseResult responseResult = scheduleClient.poll();
        if (responseResult == null || responseResult.getCode() != 200) {
            log.warn("消费失败");
            return;
        }

        Object data = responseResult.getData();
        if (data == null) {
            log.warn("没有可消费的任务");
            return;
        }

        String string = JSON.toJSONString(data);
        if (StringUtils.isEmpty(string)) {
            log.warn("没有可消费的任务2");
            return;
        }

        Task task = JSON.parseObject(string, Task.class);
        if (task == null) {
            log.warn("没有可消费的任务");
            return;
        }

        byte[] parameters = task.getParameters();
        WmNews wmNews = ProtostuffUtil.deserialize(parameters, WmNews.class);
        if (wmNews == null) {
            log.warn("参数缺失");
            return;
        }

        try {
            autoScanWmNews(wmNews.getId().longValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
