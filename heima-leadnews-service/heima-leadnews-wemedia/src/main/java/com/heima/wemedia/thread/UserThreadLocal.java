package com.heima.wemedia.thread;

import com.heima.model.wemedia.pojos.WmUser;

/**
 * @author itheima
 * @since 2022-07-15
 */
public class UserThreadLocal {

    // ThreadLocal类型的静态变量
    private static ThreadLocal<WmUser> threadLocal = new ThreadLocal<>();

    // 变量对应的get和set方法
    public static WmUser get() {
        return threadLocal.get();
    }

    public static void set(WmUser wmUser) {
        threadLocal.set(wmUser);
    }

    // 变量对应的remove方法
    public static void remove() {
        threadLocal.remove();
    }
}
