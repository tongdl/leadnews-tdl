package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.file.MinIoTemplate;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import com.heima.wemedia.thread.UserThreadLocal;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;


@Slf4j
@Service
@Transactional
public class WmMaterialServiceImpl extends ServiceImpl<WmMaterialMapper, WmMaterial> implements WmMaterialService {

    @Autowired
    private MinIoTemplate minIoTemplate;

    @Autowired
    private WmMaterialMapper wmMaterialMapper;

    @Override
    public ResponseResult upload(MultipartFile multipartFile) throws IOException {

        // 1. 获取上传图片的输入流
        InputStream inputStream = multipartFile.getInputStream();

        // image/png
        // 根据 / 进行分割
        // 获取分割之后的后面那段内容
        String contentType = multipartFile.getContentType();
        if (StringUtils.isEmpty(contentType)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
        }

        // {"image", "png"}
        String[] split = contentType.split("/");

        // 后缀名
        String suffixName = split[1];

        // 文件名
        String fileName = System.currentTimeMillis() + "." + suffixName;

        // 2. 把图片上传到minio
        String url = minIoTemplate.uploadFile("", fileName, inputStream, "image");

        // 3. 将上传后的url，写入到数据库 material这张表
        WmMaterial material = new WmMaterial();
        material.setUserId(UserThreadLocal.get().getId());
        material.setUrl(url);
        material.setIsCollection((short) 0);
        material.setType((short) 0);
        material.setCreatedTime(new Date());
        int insertResult = wmMaterialMapper.insert(material);
        if (insertResult < 1) {
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
        }

        // 4. 将创建好的素材信息，返回给前端
        return ResponseResult.okResult(material);
    }

    @Override
    public PageResponseResult list(WmMaterialDto dto) {
        if (dto == null) {
            return new PageResponseResult(0, 0, 0);
        }

        // 1. 构建查询条件
        LambdaQueryWrapper<WmMaterial> wrapper = new LambdaQueryWrapper<>();

        // 1.1 当isCollection = 1的时候，才让这个条件生效
        wrapper.eq(dto.getIsCollection() == 1, WmMaterial::getIsCollection, dto.getIsCollection());
        wrapper.orderByDesc(WmMaterial::getCreatedTime);
        // 2. 构建分页条件
        Integer currentPage = dto.getPage();
        Integer size = dto.getSize();
        Page<WmMaterial> page = new Page<>(currentPage, size);

        // 3. 执行分页查询
        Page pageResult = wmMaterialMapper.selectPage(page, wrapper);

        // 4. 构建返回值结构
        Long total = pageResult.getTotal();
        PageResponseResult pageResponseResult = new PageResponseResult(currentPage, size, total.intValue());
        pageResponseResult.setData(pageResult.getRecords());

        return pageResponseResult;
    }


}
