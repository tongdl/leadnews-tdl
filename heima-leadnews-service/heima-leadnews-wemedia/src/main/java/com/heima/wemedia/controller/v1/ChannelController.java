package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.ChannelDto;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.service.WmChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author itheima
 * @since 2022-07-15
 */
@RestController
@RequestMapping("/api/v1/channel")
public class ChannelController {

    @Autowired
    private WmChannelService wmChannelService;

    @GetMapping("/channels")
    public ResponseResult channels() {
        return wmChannelService.channelList();
    }

    @PostMapping("/save")
    public ResponseResult save(@RequestBody WmChannel wmChannel) {
        return wmChannelService.channelSave(wmChannel);
    }

    @PostMapping("/list")
    public PageResponseResult list(@RequestBody ChannelDto dto) {
        return wmChannelService.channelList2(dto);
    }

    @PostMapping("/update")
    public ResponseResult update(@RequestBody WmChannel wmChannel) {
        return wmChannelService.channelUpdate(wmChannel);
    }

    @GetMapping("/del/{id}")
    public ResponseResult del(@PathVariable Integer id) {
        return wmChannelService.channelDelete(id);
    }
}
