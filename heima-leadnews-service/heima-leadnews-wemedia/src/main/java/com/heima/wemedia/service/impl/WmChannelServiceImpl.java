package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.ChannelDto;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.utils.AppJwtUtil;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.service.WmChannelService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
@Transactional
@Slf4j
public class WmChannelServiceImpl extends ServiceImpl<WmChannelMapper, WmChannel> implements WmChannelService {

    @Autowired
    private WmChannelMapper wmChannelMapper;

    @Autowired
    private WmNewsMapper wmNewsMapper;

    @Override
    public ResponseResult channelList() {
        // 1. 构造分页（这一步主要是为了安全考虑，限制这个接口查询数据的最大值）
        Page<WmChannel> page = new Page<>(1, 100);

        // 2. 列表查询
        Page<WmChannel> wmChannelPage = wmChannelMapper.selectPage(page, null);

        // 3. 返回数据
        return ResponseResult.okResult(wmChannelPage.getRecords());
    }

    @Override
    public ResponseResult channelSave(WmChannel channel) {
        if (channel == null || channel.getOrd() < 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        String name = channel.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE, "频道名不能为空");
        }

        LambdaQueryWrapper<WmChannel> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(WmChannel::getName, name);
        Integer count = wmChannelMapper.selectCount(wrapper);
        if (count > 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_EXIST, "频道不能重名");
        }

        channel.setCreatedTime(new Date());
        boolean saveResult = this.save(channel);

        return saveResult ? ResponseResult.okResult(AppHttpCodeEnum.SUCCESS) : ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
    }

    @Override
    public PageResponseResult channelList2(ChannelDto dto) {
        if (dto == null) {
            return new PageResponseResult(0, 0, 0);
        }

        Integer page = dto.getPage();
        Integer size = dto.getSize();

        page = page == null ? 1 : page;
        size = size == null ? 10 : size;

        Page<WmChannel> pageInfo = new Page<>(page, size);

        String name = dto.getName();
        LambdaQueryWrapper<WmChannel> wrapper = new LambdaQueryWrapper<>();
        wrapper.like(StringUtils.isNotBlank(name), WmChannel::getName, name);
        wrapper.orderByDesc(WmChannel::getCreatedTime);

        Page<WmChannel> pageResult = wmChannelMapper.selectPage(pageInfo, wrapper);

        Long total = pageResult.getTotal();
        PageResponseResult pageResponseResult = new PageResponseResult(page, size, total.intValue());
        pageResponseResult.setData(pageResult.getRecords());

        return pageResponseResult;
    }

    @Override
    public ResponseResult channelUpdate(WmChannel wmChannel) {
        if (wmChannel == null || wmChannel.getOrd() < 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        LambdaQueryWrapper<WmNews> newsWrapper = new LambdaQueryWrapper<>();
        newsWrapper.eq(WmNews::getChannelId, wmChannel.getId());
        newsWrapper.eq(WmNews::getStatus, 9);
        Integer newsCount = wmNewsMapper.selectCount(newsWrapper);
        if (newsCount > 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE, "频道关联了新闻，无法修改");
        }

        boolean updateResult = this.updateById(wmChannel);

        return updateResult ? ResponseResult.okResult(AppHttpCodeEnum.SUCCESS) : ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
    }

    @Override
    public ResponseResult channelDelete(Integer id) {
        if (id == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        WmChannel wmChannel = wmChannelMapper.selectById(id);
        if (wmChannel == null) {
            return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
        }

        Boolean status = wmChannel.getStatus();
        if (status) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID, "频道为启动状态，不能删除");
        }

        LambdaQueryWrapper<WmNews> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(WmNews::getChannelId, wmChannel.getId());
        wrapper.eq(WmNews::getStatus, 9);
        Integer newsCount = wmNewsMapper.selectCount(wrapper);
        if (newsCount > 0) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE, "频道关联了新闻，无法修改");
        }


        boolean removeResult = this.removeById(id);
        return removeResult ? ResponseResult.okResult(AppHttpCodeEnum.SUCCESS) : ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR);
    }
}