package com.heima.wemedia;

import com.alibaba.fastjson.JSON;
import com.heima.apis.schedule.IScheduleClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dto.Task;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author itheima
 * @since 2022-07-26
 */
@SpringBootTest
public class TaskTest {

    @Autowired
    private IScheduleClient iScheduleClient;

    @Test
    public void pollTest() {

        ResponseResult responseResult = iScheduleClient.poll();

        Object data = responseResult.getData();

        String str = JSON.toJSONString(data);

        Task task = JSON.parseObject(str, Task.class);

        System.out.println(str);

    }
}
