package com.heima.wemedia;

import com.heima.apis.article.IArticleClient;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;

/**
 * @author itheima
 * @since 2022-07-17
 */
@SpringBootTest
public class ArticleTest {

    @Autowired
    private IArticleClient articleClient;

    @Test
    public void test() {
        ArticleDto dto = new ArticleDto();
        dto.setTitle("黑马头条项目背景22222222222222");
        dto.setAuthorId(1102L);
        dto.setLayout((short) 1);
        dto.setLabels("黑马头条");
        dto.setPublishTime(new Date());
        dto.setImages("http://192.168.200.130:9000/leadnews/2021/04/26/5ddbdb5c68094ce393b08a47860da275.jpg");
        dto.setContent("22222222222222222黑马头条项目背景,黑马头条项目背景,黑马头条项目背景,黑马头条项目背景，黑马头条项目背景");
        ResponseResult responseResult = articleClient.saveArticle(dto);

        System.out.println(responseResult.getCode());
        System.out.println(responseResult.getData());
    }
}
