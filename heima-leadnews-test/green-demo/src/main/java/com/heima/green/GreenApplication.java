package com.heima.green;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author itheima
 * @since 2022-07-11
 */
@SpringBootApplication
public class GreenApplication {
    public static void main(String[] args) {
        SpringApplication.run(GreenApplication.class, args);
    }
}
