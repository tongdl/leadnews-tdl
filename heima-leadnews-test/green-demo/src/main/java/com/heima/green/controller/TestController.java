package com.heima.green.controller;

import com.heima.common.aliyun.GreenImageScan;
import com.heima.common.aliyun.GreenTextScan;
import com.heima.green.annotation.ShuaiShuai;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author itheima
 * @since 2022-07-11
 */
@RestController
public class TestController {

    @Autowired
    private GreenTextScan greenTextScan;

    @Autowired
    private GreenImageScan greenImageScan;

    @PostMapping("test/text")
    public Map testText(String text) throws Exception {
        return greenTextScan.greeTextScan(text);
    }

    @PostMapping("test/image")
    public Map testText(MultipartFile file) throws Exception {
        List<byte[]> list = new ArrayList<>();

        byte[] bytes = file.getBytes();

        list.add(bytes);

        return greenImageScan.imageScan(list);
    }

}
