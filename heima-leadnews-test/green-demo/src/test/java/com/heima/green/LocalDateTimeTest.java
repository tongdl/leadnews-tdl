package com.heima.green;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author itheima
 * @since 2022-07-31
 */
@SpringBootTest
public class LocalDateTimeTest {

    @Test
    public void test() {
        LocalDateTime now = LocalDateTime.now();

        System.out.println(now);

        String format = now.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));

        System.out.println(format);

        LocalDateTime localDateTime = now.plusDays(2);

    }

}
