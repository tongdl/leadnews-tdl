package com.heima.stream.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class TestListener {

    @KafkaListener(topics = "itcast-topic-out")
    public void onMessage(String message) {
        System.out.println(message);
    }

}