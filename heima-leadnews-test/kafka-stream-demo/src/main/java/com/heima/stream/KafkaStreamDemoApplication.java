package com.heima.stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author itheima
 * @since 2022-07-24
 */
@SpringBootApplication
public class KafkaStreamDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(KafkaStreamDemoApplication.class, args);
    }
}
