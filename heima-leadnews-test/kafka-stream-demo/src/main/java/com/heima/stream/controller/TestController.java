package com.heima.stream.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    // itheima, itcast, aaa
    @GetMapping("")
    public String test(String message) {
        kafkaTemplate.send("itcast-topic-input", message);
        return "ok";
    }
}