package com.heima.freemarker;

import com.heima.common.freemarker.FreemarkerGenerator;
import com.heima.file.MinIoTemplate;
import com.heima.freemarker.demo.FreemarkerDemoApplication;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author itheima
 * @since 2022-07-14
 */
@SpringBootTest(classes = FreemarkerDemoApplication.class)
public class FreemarkerTest {

    @Autowired
    private Configuration configuration;

    @Autowired
    private FreemarkerGenerator freemarkerGenerator;

    @Autowired
    private MinIoTemplate minIoTemplate;

    @Test
    public void test2() {

        // 1. 生成html文件
        InputStream is = freemarkerGenerator.generate("01-basic.ftl", getData());

        // 2. html文件上传到minio
        String url = minIoTemplate.uploadFile("", "1.html", is, "html");

        System.out.println(url);
    }


    @Test
    public void test() throws IOException, TemplateException {

        // 0. 想要提升生成html页面，将生成的html页面存放在当前项目目录中

        // 1. 导入Configuration依赖 选择freemarker的包

        // 2. 指定模板
        Template template = configuration.getTemplate("01-basic.ftl");

        // 3. 分配数据
        Map data = getData();

        FileWriter fileWriter = new FileWriter("test.html");
        // 4. 生成文件
        template.process(data, fileWriter);
    }

    private Map getData() {
        Map<String, Object> result = new HashMap<>();
        result.put("name", "itheima");

        Map<String, Object> map = new HashMap<>();
        map.put("name", "itcast");
        map.put("age", 18);

        result.put("stu", map);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("name", "itheima");
        map2.put("age", 20);

        List<Map<String, Object>> list = new ArrayList<>();
        list.add(map);
        list.add(map2);

        result.put("stus", list);

        result.put("today", new Date());

        return result;
    }

}
