package com.heima.freemarker.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author itheima
 * @since 2022-07-04
 */
@SpringBootApplication
public class FreemarkerDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(FreemarkerDemoApplication.class, args);
    }
}
