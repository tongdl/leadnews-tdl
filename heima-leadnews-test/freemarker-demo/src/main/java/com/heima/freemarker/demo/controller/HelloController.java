package com.heima.freemarker.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author itheima
 * @since 2022-07-04
 */
@Controller
public class HelloController {

    @GetMapping("/hello")
    public String hello(Model model) {

        model.addAttribute("name", "itheima");

        Map<String, Object> map = new HashMap<>();
        map.put("name", "itcast");
        map.put("age", 18);

        model.addAttribute("stu", map);

        Map<String, Object> map2 = new HashMap<>();
        map2.put("name", "itheima");
        map2.put("age", 20);

        List<Map<String, Object>> list = new ArrayList<>();
        list.add(map);
        list.add(map2);

        model.addAttribute("stus", list);

        model.addAttribute("today", new Date());

        // 视图模板的文件名 view 模板文件名
        return "01-basic";
    }

}
