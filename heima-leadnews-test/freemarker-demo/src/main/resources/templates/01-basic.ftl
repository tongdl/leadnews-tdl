<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Hello World!</title>
</head>
<body>
<b>普通文本 String 展示：</b><br><br>
Hello ${name} <br>
<hr>
<b>对象Student中的数据展示：</b><br/>
姓名：${stu.name}<br/>
年龄：${stu.age}
<hr>


<#-- 1. 集合遍历 -->
<!-- for(Object stu : stus) -->
<#if stus??>

    列表总长度为：${stus?size}

<#-- stus不为空的情况下，才会执行这里面的代码 -->
    <#list stus as stu>

    <#-- 判空 -->
        <#if (stu.age < 20) || (stu.name = 'itheima')>
        <p style="color:royalblue">${stu.name} - ${stu.age + 5}</p>
        <#else>
        <p>${stu.name} - ${stu.age - 5}</p>
        </#if>

    </#list>
</#if>


<#-- 内置函数（获取长度、时间格式）-->
日期：${today?date}<br/>
时间：${today?time}<br/>
日期+时间：${today?datetime}<br/>
格式化：${today?string('yyyy年MM月')}<br/>


<#-- 2. if else -->


<#-- 3. 运算符 -->


</body>
</html>