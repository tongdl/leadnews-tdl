package com.heima.listener;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

/**
 * @author itheima
 * @since 2022-07-23
 */
@Component
public class TestListener {
//
//    @KafkaListener(topics = "test-topic",groupId = "1001")
//    public void getMessage(String message) {
//        System.out.println("我是1号");
//        System.out.println(message);
//    }
//
//    @KafkaListener(topics = "test-topic", groupId = "1002")
//    public void getMessage2(String message) {
//        System.out.println("我是2号");
//        System.out.println(message);
//    }

    @KafkaListener(topics = "test-topic")
    public void onMessage2(ConsumerRecord<String, String> record, Acknowledgment acknowledgment) {
        System.out.println(record.value());
        acknowledgment.acknowledge();
    }

}
